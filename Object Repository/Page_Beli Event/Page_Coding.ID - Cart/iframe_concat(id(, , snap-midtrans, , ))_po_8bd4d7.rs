<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iframe_concat(id(, , snap-midtrans, , ))_po_8bd4d7</name>
   <tag></tag>
   <elementGuidId>ceec9417-83fa-4920-9ca5-4278e525bfb3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#snap-midtrans</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//iframe[@id='snap-midtrans']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>iframe</value>
      <webElementGuid>ee00edd2-4f2d-44e7-965c-1457d1668c40</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://app.sandbox.midtrans.com/snap/v1/pay?origin_host=https://demo-app.online&amp;digest=68c5ea97a4c5d6ec15db5144d4c829cd278afc4ba4faefecd3444ecf3c2692a8&amp;client_key=SB-Mid-client-aQuCzf_xly4hqcSY#/</value>
      <webElementGuid>3c21706a-ab56-41e9-8bca-aa9bba282d6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>snap-midtrans</value>
      <webElementGuid>7a95ecdd-1ca6-4cd7-8b56-ee8ddb2bf6f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>popup_1675231566170</value>
      <webElementGuid>eb4767f6-815d-417f-a4ef-a81742599594</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;snap-midtrans&quot;)</value>
      <webElementGuid>eab4990b-17c9-4280-a6a6-b9f8c740d7c3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//iframe[@id='snap-midtrans']</value>
      <webElementGuid>b1cd4582-aa3f-4cfd-af52-5707f76cfb73</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//iframe</value>
      <webElementGuid>9bf750f1-f79e-4525-ab4e-2ae583466543</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//iframe[@src = 'https://app.sandbox.midtrans.com/snap/v1/pay?origin_host=https://demo-app.online&amp;digest=68c5ea97a4c5d6ec15db5144d4c829cd278afc4ba4faefecd3444ecf3c2692a8&amp;client_key=SB-Mid-client-aQuCzf_xly4hqcSY#/' and @id = 'snap-midtrans' and @name = 'popup_1675231566170']</value>
      <webElementGuid>5aa4a943-bcda-4b16-aa36-1cbf59545277</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
