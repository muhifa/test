<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Pilih minimal 1 event sebelum checkout</name>
   <tag></tag>
   <elementGuidId>e3b71677-a043-45f9-ab33-fbada3ac1a71</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#modalEvent > div.modal-dialog > div.modal-content > div.modal-body</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='modalEvent']/div/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>02d1c446-d099-4657-ac04-d4563a6eafc4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-body</value>
      <webElementGuid>cf0edf21-f719-4bf7-a2de-c510f2dfca93</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Pilih minimal 1 event sebelum checkout
                    </value>
      <webElementGuid>fb27e63a-4f08-46ce-b746-e217f4b97728</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;modalEvent&quot;)/div[@class=&quot;modal-dialog&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]</value>
      <webElementGuid>0833f808-3a6f-4efa-8ccc-6a15d141e6ea</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='modalEvent']/div/div/div[2]</value>
      <webElementGuid>f26feb12-dd00-400e-b967-1000658519c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/following::div[1]</value>
      <webElementGuid>4c9dbcca-8c71-47fb-bb33-53c7d38d338f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirm'])[1]/following::div[6]</value>
      <webElementGuid>d7bf629e-b63f-4580-abb8-a4de943a9c1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ok'])[1]/preceding::div[1]</value>
      <webElementGuid>5bae7895-8e97-4007-9102-492e2a97192b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copyright © 2010-2021 Coding.ID All rights reserved.'])[1]/preceding::div[2]</value>
      <webElementGuid>e8ddcf78-c5cc-47c1-a395-743077bceb66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/div/div/div[2]</value>
      <webElementGuid>d64e9935-e9ef-4119-af49-54fe9dc986ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        Pilih minimal 1 event sebelum checkout
                    ' or . = '
                        Pilih minimal 1 event sebelum checkout
                    ')]</value>
      <webElementGuid>87c1a2ed-8c2e-40f7-b99a-974089cad140</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
