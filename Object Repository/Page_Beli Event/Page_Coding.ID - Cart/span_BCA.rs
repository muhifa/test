<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_BCA</name>
   <tag></tag>
   <elementGuidId>aac9eb50-800b-403d-8326-a7c9f1d0d38a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.bank-list-content > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div[2]/a/div/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>71d1e1a6-20ce-49a7-b4a6-55e9f9a9a581</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>BCA</value>
      <webElementGuid>6405feb6-4755-449d-bf6a-c7e1cb0a9913</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;bank-list-layout&quot;]/a[@class=&quot;bank-list&quot;]/div[@class=&quot;payment-page-text&quot;]/div[@class=&quot;bank-list-content&quot;]/span[1]</value>
      <webElementGuid>eea43365-25ee-4088-a029-1ac5e694665b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Page_Beli Event/Page_Coding.ID - Cart/iframe_concat(id(, , snap-midtrans, , ))_po_b2c765</value>
      <webElementGuid>c84b53e0-647a-4550-baf8-1c6cec6fde41</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div[2]/a/div/div/span</value>
      <webElementGuid>746266eb-5e69-494e-956c-ac1e85f75851</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bank transfer'])[1]/following::span[1]</value>
      <webElementGuid>94e20874-60d1-4b23-a8e1-6f4920310364</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mandiri'])[1]/preceding::span[1]</value>
      <webElementGuid>3b33c828-4715-447f-950a-e3decbb5782f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BNI'])[1]/preceding::span[2]</value>
      <webElementGuid>f0c8fc99-2956-4242-a847-4559857aa4bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='BCA']/parent::*</value>
      <webElementGuid>5550cb69-1763-4d63-bbba-250625d20130</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div/div/span</value>
      <webElementGuid>c16eda8c-8d44-4001-8839-63fad3fc6eb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'BCA' or . = 'BCA')]</value>
      <webElementGuid>e7f95cca-23a1-445e-83ca-36e9509ce4ee</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
